# Improving Deep Neural Networks

#### 介绍
吴恩达提高深度神经网络课程中ipython notebook作业，由于coursera编程作业需要加载很长时间，所以将作业下载到本地方便练习，但可以通过本地的ipython notebook完成作业。在本地完成时会存在以下问题，coursera未使用最新版本的matplotlib sklearn numpy，所以有些方法的传参需要修改成最新的接口，或者将anaconda中的python的matplotlib sklearn numpy更新到相应的版本。具体的操作请看安装教程。对于anaconda中matplotlib sklearn numpy都有介绍，这是anaconda中各种函数库的地址[https://github.com/conda-forge](https://github.com/conda-forge)，每个函数库的更新每个项目都有更新说明

#### 软件架构
python


#### 安装教程

1. 通过国内镜像下载anaconda [https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/](https://mirrors.tuna.tsinghua.edu.cn/anaconda/archive/)，安装python环境与ipython notebook 如果网速可以也可通过anaconda官网直接下载[https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)
2. 按照anaconda官方文档打开ipython notebook [https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)
3. 完成作业

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)